$(document).ready(function() {

    $('input#login, input#password').fadeTo('slow', 0.6);

    $(function(){
        var state = true;
        $('#log_toggle').on('click', function(e){
            if(state){
                e.stopPropagation();
                $('#log_toggled').animate({opacity: "1"}, 'slow');
            } else {
                $('#log_toggled').animate({opacity: "0"}, 'slow');

            }         state = !state;

        });
    });

    $('input#login, input#password')
        .on('focus', function(){$(this).fadeTo('fast', 1);})
        .on('blur', function(){ $(this).fadeTo('fast', 0.6); });




        $('li#toggle_1').on('click', function(){
            $('#toggle_box_1').animate({opacity: "1"}, 'slow');
            $('#toggle_box_2').animate({opacity: "0"}, 'slow');
            $('#toggle_box_3').animate({opacity: "0"}, 'slow');

        });

        $('li#toggle_2').on('click', function(){
            $('#toggle_box_1').animate({opacity: "0"}, 'slow');
            $('#toggle_box_2').animate({opacity: "1"}, 'slow');
            $('#toggle_box_3').animate({opacity: "0"}, 'slow');
        });

        $('#toggle_3').on('click', function(){
            $('#toggle_box_1').animate({opacity: "0"}, 'slow');
            $('#toggle_box_2').animate({opacity: "0"}, 'slow');
            $('#toggle_box_3').animate({opacity: "1"}, 'slow');
        });

    $('#login').on('keyup', function(keyup){
        if((keyup.which < 48 || keyup.which > 90)
                && (keyup.which < 96 || keyup.which > 103)
                && (keyup.which < 18 || keyup.which > 40)
                && (keyup.which < 33 || keyup.which > 40)
                && keyup.which != 189
                && keyup.which != 144
                && keyup.which != 145
                && keyup.which != 45
                && keyup.which != 46
                && keyup.which != 27
                && keyup.which != 8
                && keyup.which != 9
                && keyup.which != 13
                && keyup.which != 19
                && keyup.which != 20

        ) {
            var w = $(this).val().slice(0,-1);
            $(this).val(w);
        }
    });


});