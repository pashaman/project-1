<?php
class Controller {

    protected  $data= array ();

    public function set($name, $value) {
        $this->data[$name] = $value;
    }

    public  function delete($name){
        unset($this->data[$name]);
    }

    public function  __get($name){
        if (isset($this->data[$name])) {return $this->data[$name];}
    }

    public function loadView ($view_name, $data) {
        $template = new Template(Config::base_url.'/application/view/');
        $template->initData($data);
        $template->display($view_name);
    }
    protected function loadModel($model){
        require_once Config::base_url.'/application/model/'.$model.'.php';
        $modelx = ucfirst($model);
        $this->data[$model] = new $modelx();
            }
    public function redirect($way){
        header('Location: /'.$way);
    }

}