<?php
class Protected_controller extends Controller{
    function __construct(){
        if(!isset($_SESSION['user_id'])){
            $this->redirect(Config::$no_login);
        }
    }

}