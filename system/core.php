<?php
session_start();
define('A', $_SERVER['DOCUMENT_ROOT']);
require_once A.'/system/config.php';
require_once Config::base_url.'/system/template.php';
require_once Config::base_url.'/system/model.php';
require_once Config::base_url.'/system/controller.php';
require_once Config::base_url.'/system/protected_controller.php';
require_once Config::base_url.'/system/router.php';