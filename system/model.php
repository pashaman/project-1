<?php
class Model {

    public  $data = array();

    protected $pdo;

    public function __construct(){
        $this->pdo = new PDO('mysql:host='.Config::$db_host.';dbname='.Config::$db_name, Config::$db_login, Config::$db_password, array( PDO::ATTR_PERSISTENT => true));
        $this->pdo->exec('set names utf8');

    }

    function redirect($way){
        header('Location: /'.$way);
    }


}