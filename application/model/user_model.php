<?php
class User_model extends Model{

    public  function __construct(){
        parent::__construct();
    }

    public function login($login, $password){

        $stmt = $this->pdo->prepare('SELECT * FROM users WHERE username=:login', array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $stmt->execute(array(':login' => $login));
        $row = $stmt->fetch();
        if($row['password'] === md5($password)){
            unset ($row['password']);
            return $row;
        } else {
            return FALSE;
        }
    }

}