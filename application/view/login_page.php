<!DOCTYPE html>
<html lang="en">
    <head>
        <META content="text/html" charset="utf-8" http-equiv="Content-Type">
        <title>Login</title>
        <link href="/resources/css/main.css" rel="stylesheet" />
        <script src="/resources/js/jquery-1.11.1.js"></script>
        <script src="/resources/js/default.js"></script>
    </head>
    <body>
        <div class="header">
            <div class="authorize">
                <p class="flash"><?= $_SESSION['mess']?>&nbsp;&nbsp;
                    <a href="#" id="log_toggle" class="link">Login</a>
                </p>
            </div>
        </div>
        <div class="headline"></div>
        <div id="log_toggled">
            <form method="post" class="form_container" >
                <label for="login" class="legend" >Enter your login</label>
                <input type="text" name="login" id="login" class="form" required />
                <label for="password" class="legend">Enter your password</label>
                <input type="password" name="password" id="password" class="form" required />
                <input type="submit" class="button" value="Log In" />
                <input type="reset" class="button" value="Reset" />
            </form>
        </div>
        <div class="leftmenu"></div>
        <div class="content">
        </div>
        <div class="footer">
        </div>
    </body>
</html>