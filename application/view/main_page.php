<!DOCTYPE html>
<html lang="en">
    <head>
        <META content="text/html" charset="utf-8" http-equiv="Content-Type">
        <title>Main</title>
        <link href="/resources/css/main.css" rel="stylesheet">
        <script src="/resources/js/jquery-1.11.1.js"></script>
        <script src="/resources/js/default.js"></script>
    </head>
    <body>
        <div class="header">
            <div class="authorize">
                <p class="flash"><?= $_SESSION['mess']?>&nbsp;&nbsp;
                    <a href="user/logout" id="log_toggle" class="link">Logout</a>
                </p>
            </div>
        </div>
        <div class="headline"></div>
        <div class="leftmenu">
        <ul>
          <li class="toggle_tab" id="toggle_1"></li>
          <li class="toggle_tab2" id="toggle_2"></li>
          <li class="toggle_tab3" id="toggle_3"></li>
        </ul>
            <div id="toggle_box_3"></div>
            <div id="toggle_box_2"></div>
            <div id="toggle_box_1"></div>
        </div>
        <div class="content">
        <canvas id="canvas" width="900" height="900"></canvas>
            <script>
                    var ctx = document.getElementById('canvas').getContext('2d');
                    var img = new Image();
                    img.onload = function(){
                        ctx.drawImage(img,0,0,900,600);
                    };
                    img.src = '/resources/images/bgr.png';
            </script>
        </div>
        <div class="footer"></div>
    </body>
</html>
