<?php
class User extends Controller{

    public function index() {
        $this->loadView('404', '');
    }

    public function login(){
        if (!empty($_POST['login']) && !empty($_POST['password'])){
            if(!preg_match("/^[a-z0-9_-]{3,16}$/",$_POST['login'])){
                $_SESSION['mess']='Entered login is incorrect';
                $this->loadView('login_page', '');
            }
            $login = $_POST['login'];
            $password = $_POST['password'];
            $this->loadModel('user_model');
            $user = $this->user_model->login($login, $password);
            if(!$user['userid']){
                $_SESSION['mess']='Entered password and login are incorrect';
                $this->loadView('login_page', '');
            } else {
                $_SESSION['user_id'] = $user['userid'];
                $this->redirect(Config::$after_login);
            }
        } else {
            $_SESSION['mess']='';
            $this->loadView('login_page', '');
        }
    }

    public function logout(){
        unset($_SESSION['user_id']);
        $this->redirect(Config::$no_login);
    }

}